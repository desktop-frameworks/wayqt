/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <QSize>
#include <QPoint>
#include <QObject>

struct wf_kill_view_base;
struct wf_kill_view_base_listener;

namespace WQt {
    class WayfireViewKill;
}

class WQt::WayfireViewKill : public QObject {
    Q_OBJECT;

    public:
        WayfireViewKill( wf_kill_view_base *killBase );
        ~WayfireViewKill();

        /**
         * Kill the view by sending a suitable unix signal.
         * By default, only a xdg-close request will be sent.
         * If sigNum > 0, kill( <pid>, sigNum ) will be called.
         */
        void kill( int sigNum = -1 );

        wf_kill_view_base *get();

    private:
        static void handleViewPid( void *, struct wf_kill_view_base *, int32_t );

        static void handleCancel( void *, struct wf_kill_view_base * );

        /** Raw C pointer to this class */
        wf_kill_view_base *mObj;

        /** Listener */
        static const wf_kill_view_base_listener mListener;

    Q_SIGNALS:
        /** Pid of the view */
        void pid( pid_t );

        /** The kill operation was canceled */
        void canceled();
};
