/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <QObject>
#include <QWindow>

struct wl_seat;
struct wl_surface;
struct xdg_activation_v1;
struct xdg_activation_token_v1;
struct xdg_activation_token_v1_listener;

namespace WQt {
    class XdgActivation;
    class XdgActivationToken;
}

class WQt::XdgActivation : public QObject {
    Q_OBJECT;

    public:
        XdgActivation( xdg_activation_v1 * );
        ~XdgActivation();

        XdgActivationToken * getActivationToken();
        void activate( QString, QWindow * );

        xdg_activation_v1 *get();

    private:
        xdg_activation_v1 *mObj;
};


class WQt::XdgActivationToken : public QObject {
    Q_OBJECT;

    public:
        XdgActivationToken( xdg_activation_token_v1 * );
        ~XdgActivationToken();

        void setSerial( uint, wl_seat * );
        void setAppId( QString );
        void setSurface( QWindow * );

        void commit();

        xdg_activation_token_v1 *get();

    private:
        static void handleDone( void *data, struct xdg_activation_token_v1 *token, const char *tokenStr );

        xdg_activation_token_v1 *mObj;
        static const xdg_activation_token_v1_listener mListener;

    Q_SIGNALS:
        void activationTokenReceived( QString );
};
