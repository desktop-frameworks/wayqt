/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <QObject>
#include <QTimer>

class QWindow;
class QScreen;
class DesQLayerSurface;
class WayfireHotSpot;
class WayfireHotSpotPrivate;

struct wl_output;
struct wl_surface;
struct zwf_shell_manager_v2;
struct zwf_output_v2;
struct zwf_output_v2_listener;
struct zwf_hotspot_v2;
struct zwf_hotspot_v2_listener;
struct zwf_surface_v2;

namespace WQt {
    class Shell;
    class WfOutput;
    class HotSpot;
    class Surface;
}

class WQt::Shell : public QObject {
    Q_OBJECT;

    public:
        Shell( zwf_shell_manager_v2 *shellMgr );
        ~Shell();

        /**
         * Get the wayfire output for the give wl_output.
         * Don't forget to call WfOutput::setup() before using it
         */
        WQt::WfOutput *getOutput( wl_output * );

        /**
         * Get the wayfire surface for the given wl_surface.
         * This surface can be used for interactive move.
         */
        WQt::Surface *getSurface( wl_surface * );

        zwf_shell_manager_v2 *get();

    private:
        zwf_shell_manager_v2 *mObj;
};

class WQt::WfOutput : public QObject {
    Q_OBJECT;

    public:
        enum HotSpotPoint {
            Top         = 1,
            Bottom      = 2,
            Left        = 4,
            Right       = 8,
            TopLeft     = Top | Left,
            TopRight    = Top | Right,
            BottomLeft  = Bottom | Left,
            BottomRight = Bottom | Right
        };

        Q_ENUM( HotSpotPoint );

        WfOutput( zwf_output_v2 *wfOutput );
        ~WfOutput();

        /**
         * We would like to give others a chance to setup signals
         */
        void setup();

        /**
         * Create a hotspot for a @location, defined by a region @threshold px
         * and the mouse stays there for atleast @timeout ms.
         */
        WQt::HotSpot *createHotSpot( HotSpotPoint location, uint32_t threshold, uint32_t timeout );

        zwf_output_v2 *get();

    private:
        zwf_output_v2 *mObj;

        static void handleEnterFullScreen( void *data, zwf_output_v2 *wfOut );
        static void handleLeaveFullScreen( void *data, zwf_output_v2 *wfOut );

        static const zwf_output_v2_listener mWfOutputListener;

    Q_SIGNALS:
        void enteredFullScreen();
        void leftFullScreen();
};

class WQt::HotSpot : public QObject {
    Q_OBJECT;

    public:
        HotSpot( zwf_hotspot_v2 * );
        ~HotSpot();

        void setup();

        zwf_hotspot_v2 *get();

    private:
        zwf_hotspot_v2 *mObj;

        static void handleEnterHotSpot( void *data, zwf_hotspot_v2 *wfHotspot );
        static void handleLeaveHotSpot( void *data, zwf_hotspot_v2 *wfHotspot );

        static const zwf_hotspot_v2_listener mWfHotSpotListener;

    Q_SIGNALS:
        void enteredHotSpot();
        void leftHotSpot();
};

class WQt::Surface : public QObject {
    Q_OBJECT;

    public:
        Surface( zwf_surface_v2 * );
        ~Surface();

        void move();

        zwf_surface_v2 *get();

    private:
        zwf_surface_v2 *mObj;
};
