/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <QObject>

struct wl_surface;
struct xdg_wm_base;
struct xdg_wm_base_listener;
struct xdg_surface;
struct xdg_toplevel;
struct xdg_popup;
struct xdg_positioner;

namespace WQt {
    class XdgShell;
    class XdgPopup;
    class XdgTopLevel;
    class XdgPositioner;
}

class WQt::XdgShell : public QObject {
    Q_OBJECT;

    public:
        XdgShell( xdg_wm_base * );
        ~XdgShell();

        WQt::XdgPositioner *createPositioner();

        /** Create XdgTopLevel object from wl_surface */
        WQt::XdgTopLevel *createTopLevel( wl_surface *surface );

        /** Create XdgPopup from wl_surface */
        WQt::XdgPopup *createPopup( wl_surface *surface, xdg_surface *parent, xdg_positioner *placer );

        xdg_wm_base *get();

    private:
        static void handlePing( void *data, struct xdg_wm_base *shell, uint32_t serial );

        xdg_wm_base *mObj;

        static const xdg_wm_base_listener mListener;
};
