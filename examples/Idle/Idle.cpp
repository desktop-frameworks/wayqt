/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QtWidgets>

#include <wayland-client.h>
#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>

#include <wayqt/Idle.hpp>
#include <wayqt/WlrIdle.hpp>


int main( int argc, char *argv[] ) {
    QApplication *app = new QApplication( argc, argv );

    WQt::Registry *reg = new WQt::Registry( WQt::Wayland::display() );

    reg->setup();

    wl_seat *seat = reg->waylandSeat();

    WQt::IdleManager  *kdeIdle = nullptr;
    WQt::IdleNotifier *wayIdle = nullptr;

    /** Wait for one of the two to become available */
    do {
        kdeIdle = reg->idleManager();
        wayIdle = reg->idleNotifier();
    } while ( (kdeIdle == nullptr) and (wayIdle == nullptr) );


    if ( kdeIdle != nullptr ) {
        WQt::IdleWatcher *timer = kdeIdle->getIdleWatcher( seat, 5000 );    // 5s-timeout idle timer.

        QObject::connect(
            timer, &WQt::IdleWatcher::timedOut, [ = ] () {
                qDebug() << "The system has been idle for 30s";
            }
        );

        QObject::connect(
            timer, &WQt::IdleWatcher::activityResumed, [ = ] () {
                qDebug() << "Activity resumed";
                app->quit();
            }
        );
    }

    else {
        qDebug() << "Using Wayland Idle Protocol";
        WQt::IdleNotification *timer = wayIdle->createNotification( 5000, seat );    // 5s-timeout idle
                                                                                     // timer.

        QObject::connect(
            timer, &WQt::IdleNotification::idled, [ = ] () {
                qDebug() << "The system has been idle for 5 seconds.";
            }
        );

        QObject::connect(
            timer, &WQt::IdleNotification::resumed, [ = ] () {
                qDebug() << "Activity resumed.";
                app->quit();
            }
        );

        timer->setup();
    }

    qDebug() << "Idle starting...";

    return app->exec();
}
