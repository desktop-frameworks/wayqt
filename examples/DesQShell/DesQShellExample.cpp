/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QtWidgets>

#include <wayland-client.h>
#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/DesQShell.hpp>

class DropDown : public QTextEdit {
    Q_OBJECT;

    public:
        DropDown() {
            setWindowFlags( Qt::Window | Qt::FramelessWindowHint );
            setFixedSize( qApp->primaryScreen()->size() * 0.81 );

            setWindowTitle( "DesQ DropDown" );
            setWindowIcon( QIcon::fromTheme( "desq-dropdown", QIcon::fromTheme( "desq-term" ) ) );
        }

        bool event( QEvent *event ) {
            if ( event->type() == QEvent::ApplicationDeactivate ) {
                qDebug() << "App lost focus. Hiding the drop down.";
                hide();
            }

            if ( event->type() == QEvent::WindowDeactivate ) {
                qDebug() << "Window lost focus. Hiding the drop down.";
                hide();
            }

            else if ( event->type() == QEvent::FocusOut ) {
                qDebug() << "Focus taken out of window. Hiding the drop down.";
                // Do not hide the view: the view may lose focus because a popup was opened!!
            }

            return QTextEdit::event( event );
        }
};

int main( int argc, char *argv[] ) {
    QApplication app( argc, argv );

    WQt::Registry *reg = new WQt::Registry( WQt::Wayland::display() );

    if ( reg ) {
        reg->setup();
    }

    QTextEdit *bg = new QTextEdit();
    DropDown  *dd = new DropDown();

    bg->setText( "\n\n\n\n\n Background" );
    bg->setWindowFlags( Qt::Window | Qt::FramelessWindowHint );
    bg->setFixedSize( app.primaryScreen()->size() );

    dd->setText( "\n\n\n\n\n Dropdown" );

    int time = 0;

    WQt::DesQShell *desqShell = nullptr;

    do {
        /** Sleep 100 ms */
        QThread::msleep( 100 );
        time += 100;

        /** Attempt to get the desq shell */
        desqShell = reg->desqShell();

        /** Don't wat more than 5000 ms (i.e. 5 seconds) */
        if ( time > 5000 ) {
            qDebug() << "DesQ Shell is taking too long to be initialized. Aborting...";
            return 0;
        }
    } while ( desqShell == nullptr );

    qDebug() << "DesQ Shell took" << time << "ms to initialize";

    if ( desqShell ) {
        // bg->show();
        // desqShell->setBackground( bg->windowHandle(), app.primaryScreen() );
        //
        dd->show();
        desqShell->setDropdown( dd->windowHandle(), app.primaryScreen() );

        WQt::DesQOutput *desqOp = desqShell->getOutput( WQt::Utils::wlOutputFromQScreen( app.primaryScreen() ) );

        QObject::connect(
            desqOp, &WQt::DesQOutput::viewAdded, [ = ] ( uint32_t uuid, int row, int column ) {
                qDebug() << "New view added:" << uuid << "on" << QString( "(%1, %2)" ).arg( row ).arg( column ).toUtf8().data();
            }
        );

        QObject::connect(
            desqOp, &WQt::DesQOutput::viewRemoved, [ = ] ( uint32_t uuid, int row, int column ) {
                qDebug() << "View removed:" << uuid << "from" << QString( "(%1, %2)" ).arg( row ).arg( column ).toUtf8().data();
            }
        );

        QObject::connect(
            desqOp, &WQt::DesQOutput::workspaceExtentsChanged, [ = ] ( uint rows, uint cols ) {
                qDebug() << "New workspace grid size:" << rows << "x" << cols;
            }
        );

        QObject::connect(
            desqOp, &WQt::DesQOutput::activeWorkspaceChanged, [ = ] ( uint row, uint col ) {
                qDebug() << "Active workspace changed:" << row << "x" << col;
            }
        );

        desqOp->setup();

        qDebug() << "Switching workspace to: 0 x 2";
        desqOp->setActiveWorkspace( 0, 2 );

        return app.exec();
    }

    qDebug() << "Unable to initialize the desq-shell protocol. Does your compositor have support for it?";

    return 0;
}


#include "DesQShellExample.moc"
