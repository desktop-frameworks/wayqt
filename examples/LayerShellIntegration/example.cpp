/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QtWidgets>

#include <wayland-client.h>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>
#include <wayqt/WayfireShell.hpp>

class FloatingButton : public QToolButton {
    Q_OBJECT;

    public:
        inline FloatingButton() : QToolButton() {
            setIconSize( QSize( 48, 48 ) );
            setIcon( QIcon::fromTheme( "transform-move" ) );

            setWindowFlags( Qt::BypassWindowManagerHint | Qt::FramelessWindowHint );

            setMouseTracking( true );
        }

        Q_SLOT inline void show() {
            QToolButton::show();

            /** This is exclusively for wayland */
            if ( WQt::Utils::isWayland() ) {
                mLyrSrf = WQt::LayerShell::forWindow( QToolButton::windowHandle() );

                /** Size of our surface */
                mLyrSrf->setSurfaceSize( size() );

                /** Nothing should disturb us */
                mLyrSrf->setExclusiveZone( 0 );

                /** We don't need keyboard interaction */
                mLyrSrf->setKeyboardInteractivity( WQt::LayerSurface::NoFocus );

                /** Commit to our choices */
                mLyrSrf->apply();
            }

            if ( mReg->waitForInterface( WQt::Registry::WayfireShellInterface ) ) {
                qDebug() << "Wayfire Shell Interface";
                wl_surface *wlSurf = WQt::Utils::wlSurfaceFromQWindow( windowHandle() );
                mSurf = mReg->wayfireShell()->getSurface( wlSurf );
            }
        }

        inline void setupWayland( WQt::Registry *reg ) {
            mReg = reg;
        }

    private:
        WQt::Registry *mReg        = nullptr;
        WQt::LayerSurface *mLyrSrf = nullptr;
        WQt::Surface *mSurf        = nullptr;

        bool mPressed = false;

    protected:
        inline void mousePressEvent( QMouseEvent *event ) override {
            if ( event->button() == Qt::LeftButton ) {
                mPressed = true;
            }

            QToolButton::mousePressEvent( event );
        }

        inline void mouseMoveEvent( QMouseEvent *event ) override {
            if ( mSurf and mPressed ) {
                mSurf->move();
            }

            QToolButton::mouseMoveEvent( event );
        }

        inline void mouseReleaseEvent( QMouseEvent *event ) override {
            mPressed = false;
            QToolButton::mouseMoveEvent( event );
        }
};

class FloatingEditor : public QTextEdit {
    Q_OBJECT;

    public:
        inline FloatingEditor() : QTextEdit() {
            setFixedSize( 640, 480 );
            setText( "The quick brown fox jumps over the lazy dog. Jackdaws love my big sphinx of quartz." );
        }

        Q_SLOT inline void show() {
            QTextEdit::show();

            /** This is exclusively for wayland */
            if ( WQt::Utils::isWayland() ) {
                mLyrSrf = WQt::LayerShell::forWindow( QTextEdit::windowHandle() );

                qDebug() << mLyrSrf;

                /** Size of our surface */
                mLyrSrf->setSurfaceSize( size() );

                /** Nothing should disturb us */
                mLyrSrf->setExclusiveZone( 0 );

                /** We don't need keyboard interaction */
                mLyrSrf->setKeyboardInteractivity( WQt::LayerSurface::OnDemand );

                /** Commit to our choices */
                mLyrSrf->apply();
            }
        }

    private:
        WQt::LayerSurface *mLyrSrf = nullptr;
        WQt::Surface *mSurf        = nullptr;
};

int main( int argc, char *argv[] ) {
    qputenv( "QT_WAYLAND_SHELL_INTEGRATION", "desq-layer-shell" );

    QApplication app( argc, argv );

    WQt::Registry *reg = new WQt::Registry( WQt::Wayland::display() );
    reg->setup();

    FloatingButton *btn = new FloatingButton();
    btn->setupWayland( reg );
    btn->show();

    // FloatingEditor *edit = new FloatingEditor();
    // edit->show();

    // auto TE = new QTextEdit();
    // TE->show();

    // auto LS = WQt::LayerShell::forWindow( TE->windowHandle() );
    // LS->apply();

    return app.exec();
}


#include "example.moc"
