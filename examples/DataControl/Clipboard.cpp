#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#include <memory>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/DataControl.hpp>

#include "Clipboard.hpp"

ClipboardManager::ClipboardManager( WQt::DataControlManager *dataManager, WQt::DataControlDevice *dataDevice, QObject *parent ) : QObject( parent ) {
    mDataManager = dataManager;
    qDebug() << 1;

    mDataDevice = std::unique_ptr<WQt::DataControlDevice>( dataDevice );
    qDebug() << 2;

    // Connect signals to handle clipboard events
    qDebug() << connect( mDataDevice.get(), &WQt::DataControlDevice::selectionOffered, this, &ClipboardManager::onSelectionOffered );
    qDebug() << connect( mDataDevice.get(), &WQt::DataControlDevice::primarySelectionOffered, this, &ClipboardManager::onPrimarySelectionOffered );

    // Initialize the data device
    mDataDevice->setup();
    qDebug() << "Data device setup to receive signals";
}


void ClipboardManager::onSelectionOffered( WQt::DataControlOffer *offer ) {
    qDebug() << "Selection offered";

    if ( !offer ) {
        qDebug() << "  => Clipboard is empty. Restoring previous clipboard data.";
        restoreData( mLastClipboardData, false );
        return;
    }

    qDebug() << "  => Attempting to store clipboard data";// << offer->get();
    saveClipboardData( offer, mLastClipboardData );
}


// Slot called when new primary selection data is offered
void ClipboardManager::onPrimarySelectionOffered( WQt::DataControlOffer *offer ) {
    qDebug() << "Primary offered";

    if ( !offer ) {
        qDebug() << "  -> Primary selection is empty. Restoring previous primary selection data.";
        restoreData( mLastPrimarySelectionData, true );
        return;
    }

    qDebug() << "  -> Attempting to store primary data";// << offer->get();
    saveClipboardData( offer, mLastPrimarySelectionData );
}


// Save clipboard or primary selection data
void ClipboardManager::saveClipboardData( WQt::DataControlOffer *offer, QMap<QString, QByteArray>& storage ) {
    QStringList mimeTypes = offer->offeredMimeTypes();

    // We are the source
    if ( mimeTypes.contains( "x-clipboard-data" ) ) {
        qDebug() << "   -- Ignoring our own offer";
        return;
    }

    storage.clear();
    for (const QString& mimeType : mimeTypes) {
        QByteArray data = offer->retrieveData( mimeType );

        if ( !data.isEmpty() ) {
            storage.insert( mimeType, data );
        }
    }

    /** We'll claim the data to be ours only if there is some data to be offered */
    if ( storage.count() ) {
        storage.insert( "x-clipboard-data", "1" );
        qDebug() << "  -- Stored data for MIME types:" << storage.keys();
    }

    else {
        qDebug() << "  -- Empty data for offer";
    }
}


// Restore the clipboard or primary-selection data
void ClipboardManager::restoreData( const QMap<QString, QByteArray>& data, bool isPrimarySelection ) {
    if ( data.isEmpty() ) {
        qDebug() << ( isPrimarySelection ? "  ->" : "  =>" ) << "No previous data available to restore.";
        return;
    }

    auto               source = mDataManager->createDataSource();
    WQt::MimeDataStore mimeData;

    for (auto it = data.begin(); it != data.end(); ++it) {
        mimeData.setData( it.key(), it.value() );
    }

    source->setSelectionData( mimeData );

    if ( isPrimarySelection ) {
        mDataDevice->setPrimarySelection( source );
    }

    else {
        mDataDevice->setSelection( source );
    }

    qDebug() << data;

    qDebug() << "Restored data.";
}


// Main Function
int main( int argc, char *argv[] ) {
    QApplication app( argc, argv );

    qDebug() << "Starting clipboard manager";

    WQt::Registry reg( WQt::Wayland::display() );
    reg.setup();

    qDebug() << "Registry is setup";

    WQt::DataControlManager *dcm     = nullptr;
    WQt::DataControlDevice  *dataDev = nullptr;

    if ( reg.waitForInterface( WQt::Registry::DataControlManagerInterface ) ) {
        dcm = reg.dataControlManager();
        qDebug() << "DataControlManager ready";
        dataDev = dcm->getDataDevice( reg.waylandSeat() );
        qDebug() << "DataDevice ready";
    }

    else {
        qDebug() << "Unable to connect to the Wlroot's DataControlManager Interface";
        return 1;
    }

    qDebug() << "Monitoring";
    ClipboardManager clipboardManager( dcm, dataDev );

    return app.exec();
}
