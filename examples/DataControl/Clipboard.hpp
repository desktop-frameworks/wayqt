#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#include <memory>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/DataControl.hpp>

class ClipboardManager : public QObject {
    Q_OBJECT;

    public:
        ClipboardManager( WQt::DataControlManager *dataManager, WQt::DataControlDevice *dataDev, QObject *parent = nullptr );

    private slots:
        // Slot called when new clipboard data is offered
        void onSelectionOffered( WQt::DataControlOffer *offer );

        // Slot called when new primary selection data is offered
        void onPrimarySelectionOffered( WQt::DataControlOffer *offer );

    private:
        // Save clipboard or primary selection data
        void saveClipboardData( WQt::DataControlOffer *offer, QMap<QString, QByteArray>& storage );

        // Restore the clipboard or primary-selection data
        void restoreData( const QMap<QString, QByteArray>& data, bool isPrimarySelection );

        // Restore clipboard data
        void restoreClipboardData();

        // Restore primary selection data
        void restorePrimarySelectionData();

        /** WQt::DataControlManager is destroyed in WQt::Registry class */
        WQt::DataControlManager *mDataManager;

        /** WQt::DataControlDevice* is created externally, but we have to manage it. */
        std::unique_ptr<WQt::DataControlDevice> mDataDevice;

        // Stores the last clipboard content
        QMap<QString, QByteArray> mLastClipboardData;

        // Stores the last primary selection content
        QMap<QString, QByteArray> mLastPrimarySelectionData;
};
