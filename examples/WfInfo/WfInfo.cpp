/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <iostream>

#include <QFont>
#include <QDebug>
#include <QLabel>
#include <QProcess>
#include <QHBoxLayout>
#include <QApplication>
#include <QFontDatabase>

#include <wayqt/Registry.hpp>
#include <wayqt/WayQtUtils.hpp>
#include <wayqt/WayfireInformation.hpp>

QWidget *getDisplayWidget( QString infoStr ) {
    QLabel *lbl = new QLabel();

    lbl->setObjectName( "infoBase" );
    lbl->setContentsMargins( QMargins( 10, 10, 10, 10 ) );

    lbl->setFont( QFontDatabase::systemFont( QFontDatabase::FixedFont ) );
    lbl->setText( infoStr );
    lbl->setTextInteractionFlags( Qt::TextSelectableByMouse );

    QWidget *base = new QWidget();

    base->setContentsMargins( QMargins() );
    base->setWindowFlags( Qt::FramelessWindowHint );
    base->setAttribute( Qt::WA_TranslucentBackground );

    QHBoxLayout *lyt = new QHBoxLayout();
    lyt->setContentsMargins( QMargins() );
    lyt->addWidget( lbl );

    base->setLayout( lyt );
    base->setStyleSheet( "#infoBase{background: rgba(0, 0, 0, 0.9); border: 1px solid gray; border-radius: 5px;}" );

    return base;
}


int main( int argc, char *argv[] ) {
    QApplication *app = new QApplication( argc, argv );

    WQt::Registry *reg = new WQt::Registry( WQt::Wayland::display() );

    reg->setup();

    WQt::WayfireInformation *info = nullptr;

    if ( reg->waitForInterface( WQt::Registry::WayfireInformationInterface ) ) {
        info = reg->wayfireInformation();
    }

    else {
        qFatal( "Wayfire information protocol not advertised by compositor. Is wf-info plugin enabled?" );
    }

    QObject::connect(
        info, &WQt::WayfireInformation::viewInfo, [ = ]( WQt::WayfireViewInfo viewInfo ) {
            QString infoStr;
            infoStr += QString( "View ID    : %1\n" ).arg( viewInfo.uuid );
            infoStr += QString( "Client PID : %1\n" ).arg( viewInfo.pid );
            infoStr += QString( "Output ID  : %1\n" ).arg( viewInfo.outputId );
            infoStr += QString( "Workspace  : (%1, %2)\n" ).arg( viewInfo.workspace.y() ).arg( viewInfo.workspace.x() );
            infoStr += QString( "App ID     : %1\n" ).arg( viewInfo.appId );
            infoStr += QString( "Title      : %1\n" ).arg( viewInfo.title );
            infoStr += QString( "Role       : %1\n" ).arg( viewInfo.role );
            infoStr += QString( "Geometry   : %1,%2 %3x%4\n" ).arg( viewInfo.position.x() ).arg( viewInfo.position.y() ).arg( viewInfo.size.width() ).arg( viewInfo.size.height() );
            infoStr += QString( "Xwayland   : %1\n" ).arg( viewInfo.isXwayland );
            infoStr += QString( "Focused    : %1" ).arg( viewInfo.hasFocus );

            qInfo() << "=================================";
            qInfo() << infoStr.toUtf8().data();
            qInfo() << "=================================";

            QWidget *info = getDisplayWidget( infoStr.trimmed() );
            info->show();
            info->setFixedSize( info->height() * 2, info->height() );

            return app->exec();
        }
    );

    info->getViewInfo();

    return app->exec();
}
