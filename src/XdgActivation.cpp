/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2023 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include "wayqt/WayQtUtils.hpp"
#include "wayqt/XdgActivation.hpp"

#include "xdg-activation-v1-client-protocol.h"

WQt::XdgActivation::XdgActivation( xdg_activation_v1 *obj ) : QObject() {
    mObj = obj;
}


WQt::XdgActivation::~XdgActivation() {
    xdg_activation_v1_destroy( mObj );
}


WQt::XdgActivationToken * WQt::XdgActivation::getActivationToken() {
    return new WQt::XdgActivationToken( xdg_activation_v1_get_activation_token( mObj ) );
}


void WQt::XdgActivation::activate( QString token, QWindow *window ) {
    wl_surface *surf = WQt::Utils::wlSurfaceFromQWindow( window );

    xdg_activation_v1_activate( mObj, token.toUtf8().constData(), surf );
}


xdg_activation_v1 * WQt::XdgActivation::get() {
    return mObj;
}


WQt::XdgActivationToken::XdgActivationToken( xdg_activation_token_v1 *token ) : QObject() {
    mObj = token;
}


WQt::XdgActivationToken::~XdgActivationToken() {
    xdg_activation_token_v1_destroy( mObj );
}


void WQt::XdgActivationToken::setSerial( uint serial, wl_seat *seat ) {
    xdg_activation_token_v1_set_serial( mObj, serial, seat );
}


void WQt::XdgActivationToken::setAppId( QString appId ) {
    xdg_activation_token_v1_set_app_id( mObj, appId.toUtf8().constData() );
}


void WQt::XdgActivationToken::setSurface( QWindow *window ) {
    wl_surface *surf = WQt::Utils::wlSurfaceFromQWindow( window );

    xdg_activation_token_v1_set_surface( mObj, surf );
}


void WQt::XdgActivationToken::commit() {
    xdg_activation_token_v1_add_listener( mObj, &mListener, this );
    xdg_activation_token_v1_commit( mObj );
}


xdg_activation_token_v1 * WQt::XdgActivationToken::get() {
    return mObj;
}


void WQt::XdgActivationToken::handleDone( void *data, struct xdg_activation_token_v1 *token, const char *tokenStr ) {
    Q_UNUSED( token );

    WQt::XdgActivationToken *xdgToken = reinterpret_cast<WQt::XdgActivationToken *>(data);
    emit xdgToken->activationTokenReceived( tokenStr );
}


const struct xdg_activation_token_v1_listener WQt::XdgActivationToken::mListener = {
    .done = handleDone,
};
