/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <wayqt/WayfireInformation.hpp>

#include "wayfire-information-client-protocol.h"

WQt::WayfireInformation::WayfireInformation( struct wf_info_base *infoBase ) {
    mObj = infoBase;

    wf_info_base_add_listener( mObj, &mListener, this );
}


WQt::WayfireInformation::~WayfireInformation() {
    wf_info_base_destroy( mObj );
}


void WQt::WayfireInformation::getViewInfo() {
    wf_info_base_view_info( mObj );
}


void WQt::WayfireInformation::getViewInfoFromUuid( uint32_t uuid ) {
    wf_info_base_view_info_id( mObj, uuid );
}


void WQt::WayfireInformation::getAllViewInfo() {
    wf_info_base_view_info_list( mObj );
}


void WQt::WayfireInformation::handleViewInfo( void *data, struct wf_info_base *, uint32_t view_id, int32_t pid, int32_t ws_x,
                                              int32_t ws_y, const char *app_id, const char *title, const char *role, int32_t x, int32_t y,
                                              int32_t width, int32_t height, int32_t is_xwayland, int32_t focused, const char *output_name,
                                              uint32_t output_id ) {
    WayfireInformation *wfInfo = reinterpret_cast<WayfireInformation *>(data);

    WayfireViewInfo info;

    info.uuid       = view_id;
    info.output     = output_name;
    info.outputId   = output_id;
    info.pid        = pid;
    info.workspace  = QPoint( ws_x, ws_y );
    info.appId      = QString::fromUtf8( app_id );
    info.title      = QString::fromUtf8( title );
    info.role       = QString::fromUtf8( role );
    info.position   = QPoint( x, y );
    info.size       = QSize( width, height );
    info.isXwayland = (bool)is_xwayland;
    info.hasFocus   = (bool)focused;

    emit wfInfo->viewInfo( info );
}


void WQt::WayfireInformation::handleDone( void *data, struct wf_info_base * ) {
    WayfireInformation *wfInfo = reinterpret_cast<WayfireInformation *>(data);

    emit wfInfo->done();
}


const struct wf_info_base_listener WQt::WayfireInformation::mListener = {
    .view_info = handleViewInfo,
    .done      = handleDone,
};
