/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QObject>
#include <QDebug>
#include <QImage>
#include <QThread>
#include <QFile>
#include <QCoreApplication>

#include <typeinfo>
#include <iostream>

#include <wayland-client.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <poll.h>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/DataControl.hpp>
#include <wlr-data-control-unstable-v1-client-protocol.h>

#include "DataControlImpl.hpp"

/**
 * MimeData
 */

WQt::MimeDataStore::MimeDataStore( const MimeDataStore& other ) {
    mimeDataMap = other.mimeDataMap;
}


WQt::MimeDataStore &WQt::MimeDataStore::operator=( const MimeDataStore& other ) {
    if ( this != &other ) { // Check for self-assignment
        mimeDataMap = other.mimeDataMap;
    }

    return *this;
}


bool WQt::MimeDataStore::hasFormat( const QString& mimeType ) const {
    return mimeDataMap.contains( mimeType );
}


QByteArray WQt::MimeDataStore::data( const QString& mimeType ) const {
    return mimeDataMap.value( mimeType );
}


void WQt::MimeDataStore::setData( const QString& mimeType, const QByteArray& data ) {
    mimeDataMap.insert( mimeType, data );
}


void WQt::MimeDataStore::removeData( const QString& mimeType ) {
    mimeDataMap.remove( mimeType );
}


void WQt::MimeDataStore::clear() {
    mimeDataMap.clear();
}


QStringList WQt::MimeDataStore::formats() const {
    return mimeDataMap.keys();
}


/**
 * Data Control Manager
 */

WQt::DataControlManager::DataControlManager( zwlr_data_control_manager_v1 *mgr ) :mObj( mgr ) {
    // Nothing to do here
}


WQt::DataControlManager::~DataControlManager() {
    // QScopedPointer will take care of this
}


WQt::DataControlSource *WQt::DataControlManager::createDataSource() {
    zwlr_data_control_source_v1 *src = zwlr_data_control_manager_v1_create_data_source( get() );

    return new WQt::DataControlSource( src );
}


WQt::DataControlDevice *WQt::DataControlManager::getDataDevice( wl_seat *seat ) {
    if ( seat == nullptr ) {
        return nullptr;
    }

    zwlr_data_control_device_v1 *dev = zwlr_data_control_manager_v1_get_data_device( get(), seat );

    return new WQt::DataControlDevice( dev );
}


zwlr_data_control_manager_v1 *WQt::DataControlManager::get() {
    return mObj.get();
}


void WQt::DataControlManager::ManagerDeleter::operator()( zwlr_data_control_manager_v1 *pointer ) const {
    if ( pointer != nullptr ) {
        zwlr_data_control_manager_v1_destroy( pointer );
    }
}


/**
 * Data Control Device
 */


WQt::DataControlDevice::DataControlDevice( zwlr_data_control_device_v1 *dev ) : mObj( dev ) {
    // Initialize unique_ptr with nullptr instead of reset
    currentOffer = nullptr;

    /** Start listening to the events of wlr_data_control_device */
    if ( dev && ( wl_proxy_get_listener( (wl_proxy *)dev ) != &mListener ) ) {
        zwlr_data_control_device_v1_add_listener( dev, &mListener, this );
    }
}


WQt::DataControlDevice::~DataControlDevice() {
    // Destructors of unique_ptr will handle cleanup
}


void WQt::DataControlDevice::setup() {
    if ( !mIsSetup ) {
        mIsSetup = true;

        if ( pendingSelection ) {
            emit selectionOffered( pendingSelection.get() );
            currentSelection = std::move( pendingSelection );
        }

        if ( pendingPrimary ) {
            emit primarySelectionOffered( pendingPrimary.get() );
            currentPrimary = std::move( pendingPrimary );
        }
    }
}


void WQt::DataControlDevice::setSelection( DataControlSource *src ) {
    if ( get() == nullptr ) {
        return;
    }

    // Use std::unique_ptr move constructor
    currentSelectionSrc.reset( std::move( src ) );

    // Set the selection to @src
    zwlr_data_control_device_v1_set_selection( get(), src ? src->get() : nullptr );
}


void WQt::DataControlDevice::setPrimarySelection( DataControlSource *src ) {
    if ( get() == nullptr ) {
        return;
    }

    // Use std::unique_ptr move constructor
    currentPrimarySrc.reset( std::move( src ) );

    // Set the primary selection to @src
    zwlr_data_control_device_v1_set_primary_selection( get(), src ? src->get() : nullptr );
}


zwlr_data_control_device_v1 *WQt::DataControlDevice::get() {
    return mObj.get();
}


void WQt::DataControlDevice::handleDataOffer( void *data, struct zwlr_data_control_device_v1 *, struct zwlr_data_control_offer_v1 *offer ) {
    // qDebug() << "     --> Received data offer" << offer;
    DataControlDevice *dev = reinterpret_cast<DataControlDevice *>( data );

    if ( dev->currentOffer ) {
        // qDebug() << "     Invalidating current offer";
        dev->currentOffer->invalidate();
    }

    // qDebug() << "     Resetting to new offer" << offer;
    dev->currentOffer.reset( offer ? new DataControlOffer( offer ) : nullptr );
}


void WQt::DataControlDevice::handleSelection( void *data, struct zwlr_data_control_device_v1 *, struct zwlr_data_control_offer_v1 *offer ) {
    // qDebug() << "     --> Received selection event with offer" << offer;
    DataControlDevice *dev = reinterpret_cast<DataControlDevice *>( data );

    // qDebug() << "     " << dev->currentOffer->get() << offer;

    // If offer is nullptr, invalidate current offer
    if ( offer == nullptr ) {
        // if ( dev->currentOffer ) {
        //     dev->currentOffer->invalidate();
        // }

        // qDebug() << "     Setting current selection offer to nullptr";
        dev->currentSelection.reset( nullptr );
    }

    // If offer is non-null, transfer from currentOffer to currentSelection
    else {
        // qDebug() << "     Setting current selection offer to current offer";
        dev->currentSelection = std::move( dev->currentOffer );
    }

    // Always emit, even for NULL offers
    if ( dev->mIsSetup ) {
        // qDebug() << "     Emitting current selection offer" << dev->currentSelection.get();
        emit dev->selectionOffered( dev->currentSelection.get() );
    }

    else {
        // qDebug() << "     Waiting to emit current primary";
        dev->pendingSelection = std::move( dev->currentSelection );
    }
}


void WQt::DataControlDevice::handlePrimarySelection( void *data, struct zwlr_data_control_device_v1 *, struct zwlr_data_control_offer_v1 *offer ) {
    // qDebug() << "     --> Received primary event with offer" << offer;
    DataControlDevice *dev = reinterpret_cast<DataControlDevice *>( data );

    // If offer is nullptr, invalidate current offer
    if ( offer == nullptr ) {
        // if ( dev->currentOffer ) {
        //     dev->currentOffer->invalidate();
        // }

        // qDebug() << "     Setting current primary offer to nullptr";
        dev->currentPrimary.reset( nullptr );
    }

    // If offer is non-null, transfer from currentOffer to currentPrimary
    else {
        // qDebug() << "     Setting current primary offer to current offer";
        dev->currentPrimary = std::move( dev->currentOffer );
    }

    // Always emit, even for NULL offers
    if ( dev->mIsSetup ) {
        // qDebug() << "     Emitting current primary offer" << dev->currentSelection.get();
        emit dev->primarySelectionOffered( dev->currentPrimary.get() );
    }

    else {
        // qDebug() << "     Waiting to emit current primary";
        dev->pendingPrimary = std::move( dev->currentPrimary );
    }
}


void WQt::DataControlDevice::handleFinished( void *data, struct zwlr_data_control_device_v1 * ) {
    /** Everything is done, and delete the DataControlDevice */
    DataControlDevice *dev = reinterpret_cast<DataControlDevice *>( data );

    dev->mObj.reset( nullptr );
}


void WQt::DataControlDevice::DeviceDeleter::operator()( zwlr_data_control_device_v1 *pointer ) const {
    if ( pointer != nullptr ) {
        zwlr_data_control_device_v1_destroy( pointer );
    }
}


const zwlr_data_control_device_v1_listener WQt::DataControlDevice::mListener = {
    handleDataOffer,
    handleSelection,
    handleFinished,
    handlePrimarySelection
};


/**
 * Data Control Source
 */


WQt::DataControlSource::DataControlSource( zwlr_data_control_source_v1 *src ) : mObj( src ) {
    if ( src && ( wl_proxy_get_listener( (wl_proxy *)get() ) != &mListener ) ) {
        zwlr_data_control_source_v1_add_listener( get(), &mListener, this );
    }

    mMimeData.clear();

    writer.reset( new PipeWriter() );
}


WQt::DataControlSource::~DataControlSource() {
    // No explicit destruction needed; handled by QScopedPointer
}


void WQt::DataControlSource::setSelectionData( const WQt::MimeDataStore& mData ) {
    // Assign the new MIME data
    mMimeData = mData;

    // Get the list of MIME types
    QStringList formats = mMimeData.formats();

    // Offer each MIME type to the Wayland source
    for (const QString& fmt : formats) {
        if ( !fmt.isEmpty() ) { // Ensure the MIME type is not empty
            zwlr_data_control_source_v1_offer( get(), fmt.toUtf8().constData() );
        }
    }
}


zwlr_data_control_source_v1 *WQt::DataControlSource::get() {
    return mObj.get();
}


void WQt::DataControlSource::handleSend( void *data, struct zwlr_data_control_source_v1 *, const char *mimeType, int32_t fd ) {
    DataControlSource *dev = reinterpret_cast<DataControlSource *>( data );

    // Emit the dataRequested signal
    emit dev->dataRequested( QString::fromUtf8( mimeType ), fd );

    // qDebug() << "     Requesting data of type" << mimeType;

    // Check if the MIME type is valid and data is available
    if ( !dev->mMimeData.hasFormat( mimeType ) ) {
        qCritical() << "     [ERROR]: No data available for MIME type" << mimeType;
        close( fd ); // Close the FD if no data is available
        return;
    }

    // Start writing the data to the FD
    dev->writer->writeMimeData( mimeType, fd, dev->mMimeData.data( mimeType ) );
}


void WQt::DataControlSource::handleCanceled( void *data, struct zwlr_data_control_source_v1 * ) {
    DataControlSource *dev = reinterpret_cast<DataControlSource *>( data );
    emit dev->canceled();

    dev->mMimeData.clear();
}


void WQt::DataControlSource::SourceDeleter::operator()( zwlr_data_control_source_v1 *pointer ) const {
    if ( pointer != nullptr ) {
        zwlr_data_control_source_v1_destroy( pointer );
    }
}


const zwlr_data_control_source_v1_listener WQt::DataControlSource::mListener = {
    handleSend,
    handleCanceled
};

/**
 * Data Control Offer
 */

WQt::DataControlOffer::DataControlOffer( zwlr_data_control_offer_v1 *offer ) : mObj( offer ), mPipeReader( new PipeReader( this ) ) {
    if ( offer != nullptr ) {
        if ( wl_proxy_get_listener( reinterpret_cast<wl_proxy *>( get() ) ) != &mListener ) {
            zwlr_data_control_offer_v1_add_listener( get(), &mListener, this );
        }
    }

    mMimeTypes.clear();
}


void WQt::DataControlOffer::invalidate() {
    mInvalidated = true;
    emit invalidated();
    mMimeTypes.clear();

    if ( mPipeReader->isRunning() ) {
        mPipeReader->requestInterruption();
        mPipeReader->quit();
        mPipeReader->wait();
    }
}


bool WQt::DataControlOffer::isValid() const {
    return !mInvalidated;
}


QByteArray WQt::DataControlOffer::retrieveData( const QString& mimeType ) {
    // Do not do anything when invalidated
    if ( mInvalidated ) {
        return QByteArray();
    }

    // Already retrieved, return the data
    if ( mPipeReader->isRetrieved( mimeType ) ) {
        return mPipeReader->data( mimeType );
    }

    // Queued, so let's move it to front
    if ( mPipeReader->isQueued( mimeType ) ) {
        mPipeReader->prioritize( mimeType );
    }

    // Not queued, so let's queue it, but first
    else {
        // Create the pipes
        int pipeFds[ 2 ];

        // Pipe creation failed
        if ( pipe( pipeFds ) != 0 ) {
            return QByteArray();
        }

        // Tell we're ready to receive the data
        zwlr_data_control_offer_v1_receive( mObj.get(), mimeType.toUtf8().constData(), pipeFds[ 1 ] );

        // Close the write end of the pipe
        close( pipeFds[ 1 ] );

        // qDebug() << "     Called zwlr_data_control_offer_v1_receive" << mimeType << "on" << pipeFds[ 1 ];

        WQt::Utils::dispatchPending();
        WQt::Utils::flushDisplay();

        // qDebug() << "     Flushed wayland display";

        // request retriueval on priority basis
        mPipeReader->readMimeData( mimeType, pipeFds[ 0 ], true );
    }

    // Create a local event loop to keep the GUI responsive
    QEventLoop loop;

    int    elapsed = 0;
    QTimer timer;

    // Multi-shot timer, each time 10ms
    timer.setSingleShot( false );
    timer.start( 10 );

    // If data is ready or we're timed out, break the event loop
    connect(
        &timer, &QTimer::timeout, [ this, &timer, &elapsed, &loop, &mimeType ] () {
            // 10 ms have elapsed
            elapsed += 10;

            // Data is ready, break the loop
            if ( mPipeReader->isRetrieved( mimeType ) ) {
                timer.stop();
                loop.quit();
            }

            // We're timedout, break the loop
            if ( elapsed >= 5000 ) {
                timer.stop();
                loop.quit();
            }
        }
    );

    // Start the local event loop
    loop.exec();

    // Attempt to return the data whether or not we have it.
    return mPipeReader->data( mimeType );
}


QStringList WQt::DataControlOffer::offeredMimeTypes() const {
    return mMimeTypes;
}


zwlr_data_control_offer_v1 * WQt::DataControlOffer::get() {
    return mObj.get();
}


void WQt::DataControlOffer::handleOffer( void *data, struct zwlr_data_control_offer_v1 *, const char *mimeType ) {
    // qDebug() << "Recieved off event from compositor for" << mimeType;
    auto *offer = reinterpret_cast<DataControlOffer *>( data );

    if ( offer->mInvalidated ) {
        return;
    }

    QString mt( mimeType );

    if ( !offer->mMimeTypes.contains( mt ) ) {
        offer->mMimeTypes << mt;
        emit offer->mimeTypeOffered( mt );

        // Create the pipes
        int pipeFds[ 2 ];

        if ( pipe( pipeFds ) != 0 ) {
            return;
        }

        zwlr_data_control_offer_v1_receive( offer->mObj.get(), mimeType, pipeFds[ 1 ] );
        close( pipeFds[ 1 ] ); // Close the write end of the pipe

        // qDebug() << "     Called zwlr_data_control_offer_v1_receive" << mimeType << "on" << pipeFds[ 1 ];

        WQt::Utils::dispatchPending();
        WQt::Utils::flushDisplay();

        // qDebug() << "     Flushed wayland display";
        offer->mPipeReader->readMimeData( mimeType, pipeFds[ 0 ] );
    }
}


void WQt::DataControlOffer::OfferDeleter::operator()( zwlr_data_control_offer_v1 *pointer ) const {
    if ( pointer != nullptr ) {
        zwlr_data_control_offer_v1_destroy( pointer );
    }
}


const zwlr_data_control_offer_v1_listener WQt::DataControlOffer::mListener = {
    handleOffer
};
