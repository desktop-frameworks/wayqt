/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QThread>
#include <QTimer>
#include <QByteArray>
#include <QSocketNotifier>
#include <QMutex>
#include <QDebug>


typedef struct data_pod_t {
    int        fd;
    QByteArray buffer;
} DataPod;


class PipeReader : public QThread {
    Q_OBJECT;

    public:
        explicit PipeReader( QObject *parent = nullptr );
        ~PipeReader();

        // Is this mimeType queued for retrieval?
        bool isQueued( const QString& mimeTye );

        // Is this mimeType retrieved?
        bool isRetrieved( const QString& mimeTye );

        // Set the file descriptor to read from
        void readMimeData( const QString& mimeType, int fd, bool prioritize = false );

        // Prioritize this mime type
        // Ensure it's queued before calling this function
        void prioritize( const QString& mimeType );

        // Get the read data
        QByteArray data( const QString& mimeType ) const;

        // Emitted when reading is complete
        Q_SIGNAL void dataReady( const QString& mimeType, const QByteArray& data );

        // Emitted on error
        Q_SIGNAL void error( const QString& mimeType, const QString& message );

    protected:
        void run() override; // Thread entry point

    private:
        QStringList mimeTypes;
        QHash<QString, DataPod> queued;
        QHash<QString, DataPod> finished;

        void readIntoPod( const QString& mimeType );
};


class PipeWriter : public QThread {
    Q_OBJECT;

    public:
        explicit PipeWriter( QObject *parent = nullptr );
        ~PipeWriter();

        // Is this mimeType queued for retrieval?
        bool isQueued( const QString& mimeTye );

        // Is this mimeType retrieved?
        bool isWritten( const QString& mimeTye );

        // Set the file descriptor to read from
        void writeMimeData( const QString& mimeType, int fd, const QByteArray& data );

    protected:
        void run() override; // Thread entry point

    private:
        QStringList mimeTypes;
        QHash<QString, DataPod> queued;
        QStringList finished;

        void writePod( const QString& mimeType );
};
