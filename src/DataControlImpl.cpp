/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QCoreApplication>
#include <QObject>
#include <QThread>
#include <QFile>
#include <QDebug>

#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <poll.h>

#include "DataControlImpl.hpp"

PipeReader::PipeReader( QObject *parent ) : QThread( parent ) {
}


PipeReader::~PipeReader() {
    if ( isRunning() ) {
        // Request an interruption
        requestInterruption();
        // Request the thread to stop
        quit();
        // Wait for the thread to finish
        wait();
    }

    // Sequentially close the all the open FDs
    for ( QString key: queued.keys() ) {
        close( queued[ key ].fd );
    }

    queued.clear();
}


bool PipeReader::isQueued( const QString& mimeType ) {
    QMutex       m;
    QMutexLocker ml( &m );

    return queued.contains( mimeType );
}


bool PipeReader::isRetrieved( const QString& mimeType ) {
    QMutex       m;
    QMutexLocker ml( &m );

    return finished.contains( mimeType );
}


void PipeReader::readMimeData( const QString& mimeType, int fd, bool prioritize ) {
    if ( fd == -1 ) {
        return;
    }

    // Already retrieved
    if ( finished.contains( mimeType ) ) {
        return;
    }

    // Queued for retrieval
    if ( queued.contains( mimeType ) ) {
        /** Start the thread if it's not running */
        if ( isRunning() == false ) {
            start();
        }

        return;
    }

    DataPod pod = {
        .fd     = fd,
        .buffer = QByteArray()
    };

    queued[ mimeType ] = pod;

    if ( prioritize ) {
        mimeTypes.prepend( mimeType );
    }

    else {
        mimeTypes << mimeType;
    }

    qDebug() << "     Queuing read for data of type" << mimeType << "from pipe" << fd;

    /** Start the thread if it's not running */
    if ( isRunning() == false ) {
        start();
    }
}


void PipeReader::prioritize( const QString& mimeType ) {
    // Get the index of the mimeType
    int idx = mimeTypes.indexOf( mimeType );

    // If it's a valid index, move it to front
    if ( idx != -1 ) {
        mimeTypes.move( idx, 0 );
    }
}


QByteArray PipeReader::data( const QString& mimeType ) const {
    if ( finished.contains( mimeType ) ) {
        return finished.value( mimeType ).buffer;
    }

    return QByteArray();
}


void PipeReader::run() {
    while ( !isInterruptionRequested() && queued.count() ) {
        /** FIFO: First in, first out */
        QString mimeType = mimeTypes.takeFirst();

        /** Read this mimeType data */
        readIntoPod( mimeType );
    }
}


void PipeReader::readIntoPod( const QString& mimeType ) {
    /** ReseSet maxRetires and current trial */
    const int maxRetries = 5;
    int       retryCount = 0;

    char buffer[ 4096 ];

    while ( !isInterruptionRequested() ) {
        ssize_t bytesRead = read( queued[ mimeType ].fd, buffer, sizeof( buffer ) );
        qDebug() << "     bytesRead" << bytesRead << mimeType;

        if ( bytesRead > 0 ) {
            queued[ mimeType ].buffer.append( buffer, bytesRead );

            // Reset retry count on successful read
            retryCount = 0;
        }

        // End of file (pipe closed by the other end)
        else if ( bytesRead == 0 ) {
            break;
        }

        else {
            if ( ( errno == EAGAIN ) || ( errno == EWOULDBLOCK ) ) {
                // No data available yet, wait for a short time (~10ms)
                msleep( 10 );
                continue;
            }

            // Error occurred (e.g., the application crashed or the pipe is invalid)
            qCritical() << "     [ERROR]: Failed to read from FD" << queued[ mimeType ].fd << ":" << strerror( errno );

            // Increment retry count
            retryCount++;

            if ( retryCount >= maxRetries ) {
                // Max retries reached, emit error and quit
                emit error( mimeType, QString( "Failed to read from pipe after %1 retries: %2" ).arg( maxRetries ).arg( strerror( errno ) ) );
                break;
            }

            // Wait before retrying
            qDebug() << "     [DEBUG]: Retrying read data for" << mimeType << QString( "(%1/%2)..." ).arg( retryCount ).arg( maxRetries ).toUtf8().data();
            msleep( 50 ); // Wait ~50ms before retrying
        }
    }

    // Close the FD
    close( queued[ mimeType ].fd );

    // Emit the finished signal with the read data
    emit dataReady( mimeType, queued[ mimeType ].buffer );

    // Store for potential future use
    finished[ mimeType ] = queued.take( mimeType );
}


/**
 * PipeWriter class
 */


PipeWriter::PipeWriter( QObject *parent ) : QThread( parent ) {
    /**
     * In the current implementation,
     * we don't have any thing to initialize.
     * This behaviour can change at a later stage.
     */
}


PipeWriter::~PipeWriter() {
    if ( isRunning() ) {
        // Request an interruption
        requestInterruption();
        // Request the thread to stop
        quit();
        // Wait for the thread to finish
        wait();
    }

    // Sequentially close the all the open FDs
    for ( QString key: queued.keys() ) {
        close( queued[ key ].fd );
    }

    queued.clear();
}


void PipeWriter::writeMimeData( const QString& mimeType, int fd, const QByteArray& data ) {
    if ( fd == -1 ) {
        return;
    }

    // Already written
    if ( finished.contains( mimeType ) ) {
        return;
    }

    // Queued for writing
    if ( queued.contains( mimeType ) ) {
        /** Start the thread if it's not running */
        if ( isRunning() == false ) {
            start();
        }

        return;
    }

    // Create the pod and queue it
    DataPod pod = {
        .fd     = fd,
        .buffer = data,
    };

    queued[ mimeType ] = pod;
    mimeTypes << mimeType;

    qDebug() << "     Queuing write for data of type" << mimeType << "into pipe" << fd;

    /** Start the thread if it's not running */
    if ( isRunning() == false ) {
        start();
    }
}


void PipeWriter::run() {
    while ( !isInterruptionRequested() && queued.count() ) {
        /** FIFO: First in, first out */
        QString mimeType = mimeTypes.takeFirst();

        /** Write data of this mimeType */
        writePod( mimeType );
    }
}


void PipeWriter::writePod( const QString& mimeType ) {
    if ( queued[ mimeType ].fd < 0 ) {
        qWarning() << "     [ERROR]: Invalid file descriptor.";
        return;
    }

    // Pointer to the data
    const char *buffer = queued[ mimeType ].buffer.constData();

    // Total bytes to write
    qint64 bytesToWrite = queued[ mimeType ].buffer.size();

    // Maximum number of retries
    int retries = 5;

    // Write loop: try 5 times
    while ( bytesToWrite > 0 && retries > 0 ) {
        // Attempt to write the data
        ssize_t bytesWritten = write( queued[ mimeType ].fd, buffer, bytesToWrite );

        if ( bytesWritten < 0 ) {
            // Error occurred during write
            qWarning() << "     [ERROR]: Write error:" << strerror( errno );
            retries--;
            continue;
        }

        // Update the buffer pointer and remaining bytes
        buffer       += bytesWritten;
        bytesToWrite -= bytesWritten;

        if ( bytesToWrite > 0 ) {
            qDebug() << "     Partial write:" << bytesWritten << "bytes. Retrying...";
            retries--;
        }
    }

    if ( bytesToWrite > 0 ) {
        qWarning() << "     [ERROR]: Failed to write all data after 5 retries.";
    }
    else {
        qDebug() << "     Data written successfully.";
    }

    // Success or failure, we close the FD.
    close( queued[ mimeType ].fd );

    // Remove this from the queue
    queued.remove( mimeType );

    // Add this mimeType to finished list
    finished << mimeType;
}
